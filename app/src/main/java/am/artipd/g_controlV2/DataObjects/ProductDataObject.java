package am.artipd.g_controlV2.DataObjects;

import java.io.Serializable;

public class ProductDataObject implements Serializable {
    public String getProductName() {
        return productName;
    }

    public String getProductQrCode() {
        return productQrCode;
    }

    public String getProductArticule() {
        return productArticule;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public double getProductCoast() {
        return productCoast;
    }

    public double getProductSelfCoast() {
        return productSelfCoast;
    }

    public double getProductInStockCount() {
        return productInStockCount;
    }

    public double getProductReservCount() {
        return productReservCount;
    }

    public double getProductCountLimitation() {
        return productCountLimitation;
    }

    String productName, productQrCode, productArticule, productDescription;
    double productCoast, productSelfCoast, productInStockCount, productReservCount, productCountLimitation;
    int ID;

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public ProductDataObject(int ID, String productName, String productQrCode, String productArticule,
                             String productDescription, double productCoast, double productSelfCoast,
                             double productInStockCount, double productReservCount, double productCountLimitation) {
        this.productName = productName;
        this.productQrCode = productQrCode;
        this.productArticule = productArticule;
        this.productDescription = productDescription;
        this.productCoast = productCoast;
        this.productSelfCoast = productSelfCoast;
        this.productInStockCount = productInStockCount;
        this.productReservCount = productReservCount;
        this.productCountLimitation = productCountLimitation;
        this.ID = ID;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductQrCode(String productQrCode) {
        this.productQrCode = productQrCode;
    }

    public void setProductArticule(String productArticule) {
        this.productArticule = productArticule;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setProductCoast(double productCoast) {
        this.productCoast = productCoast;
    }

    public void setProductSelfCoast(double productSelfCoast) {
        this.productSelfCoast = productSelfCoast;
    }

    public void setProductInStockCount(double productInStockCount) {
        this.productInStockCount = productInStockCount;
    }

    public void setProductReservCount(double productReservCount) {
        this.productReservCount = productReservCount;
    }

    public void setProductCountLimitation(double productCountLimitation) {
        this.productCountLimitation = productCountLimitation;
    }
}
