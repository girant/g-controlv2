package am.artipd.g_controlV2.DataObjects;

import org.json.JSONException;
import org.json.JSONObject;

public class ProductDataJson {

    public String getProductName() {
        return productName;
    }

    public String getProductQrCode() {
        return productQrCode;
    }

    public String getProductArticule() {
        return productArticule;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public double getProductCoast() {
        return changeToDouble(productCoast);
    }
    public double getProductSelfCoast() {
        return changeToDouble(productSelfCoast);
    }

    public double getProductInStockCount() {
        return changeToDouble(productInStockCount);
    }

    public double getProductReservCount() {
        return changeToDouble(productReservCount);
    }

    public double getProductCountLimitation() {
        return changeToDouble(productCountLimitation);
    }

    String productName, productQrCode, productArticule, productDescription, jsonObject,
            productCoast, productSelfCoast, productInStockCount, productReservCount, productCountLimitation;
    JSONObject json;
    int ID;

    public void setID(int ID) {
        this.ID = ID;
    }

    public JSONObject getJsonObject(){
        return json;
    }

    public int getID() {
        return ID;
    }

    public String getJsonToSttring(){
        return jsonObject;
    }

    public ProductDataJson (JSONObject jsonObject) throws JSONException {
        this.json = jsonObject;
        this.jsonObject = jsonObject.toString();
        try {
            productName = jsonObject.getString("name");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        productQrCode = jsonObject.getString("barcode");
        productArticule = jsonObject.getString("vendorCode");
        productDescription = jsonObject.getString("description");
        productCoast = jsonObject.getString("cost");
        productSelfCoast = jsonObject.getString("primeCost");
        productInStockCount = jsonObject.getString("number");
        productReservCount = jsonObject.getString("numberReserved");
        productCountLimitation = jsonObject.getString("minNumber");
        ID = jsonObject.getInt("id");

    }

    public void setProductName(String productName) {
        this.productName = productName;
        try {
            json.put("name", productName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setProductQrCode(String productQrCode) {
        this.productQrCode = productQrCode;
        try {
            json.put("barcode", productQrCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setProductArticule(String productArticule) {
        this.productArticule = productArticule;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setProductCoast(double productCoast) {
        this.productCoast = String.valueOf(productCoast);
        try {
            json.put("cost", getProductCoast());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setProductSelfCoast(double productSelfCoast) {
       // this.productSelfCoast = productSelfCoast;
    }

    public void setProductInStockCount(double productInStockCount) {
        this.productInStockCount = String.valueOf(productInStockCount);
        try {
            json.put("number", getProductInStockCount());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setProductReservCount(double productReservCount) {
      //  this.productReservCount = productReservCount;
    }

    public void setProductCountLimitation(double productCountLimitation) {
      //  this.productCountLimitation = productCountLimitation;
    }

    private Double changeToDouble(String value){
        if (!value.equals("null")){
            return Double.valueOf(value);
        }
        else return 0d;
    }
}

