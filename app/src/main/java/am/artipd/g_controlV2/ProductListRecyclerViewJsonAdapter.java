package am.artipd.g_controlV2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import am.artipd.g_controlV2.DataObjects.ProductDataJson;
import am.artipd.g_controlV2.DataObjects.ProductDataObject;

public class ProductListRecyclerViewJsonAdapter extends RecyclerView.Adapter<ProductListRecyclerViewJsonAdapter.RecyclerViewHolder> implements PopupMenu.OnMenuItemClickListener {
    int lastPosition = -1;

    private List<ProductDataJson> viewModel;

    public ProductListRecyclerViewJsonAdapter(List<ProductDataJson> viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.to_inflate_products_to_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        ProductDataJson fbModelAtPosition = viewModel.get(position);
        holder.tvName.setText(fbModelAtPosition.getProductName());
        holder.tvCount.setText("Count" + ": " + fbModelAtPosition.getProductInStockCount() + " ");
        holder.tvCoast.setText("Cost" + ": " + (fbModelAtPosition.getProductCoast()));
        View v = holder.itemView.findViewById(R.id.relLayoutInflated);
        v.setOnCreateContextMenuListener((menu, view, contextMenuInfo) -> {
            menu.add(holder.getAdapterPosition(), 0, 0, "Delete");
            menu.add(holder.getAdapterPosition(), 1, 0, "Edit");
            menu.add(holder.getAdapterPosition(), 2, 0, "Take");

        });

        int animId;
        if (position > lastPosition) {
            animId = R.anim.up_from_bottom;

        } else {
            animId = R.anim.down_from_top;

        }
        lastPosition = position;
        Animation animation = AnimationUtils.loadAnimation(holder.itemView.getContext(), animId);
        holder.itemView.startAnimation(animation);
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return viewModel.size();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {

        Log.d("ITEM SELECTED", "ITEM SELECTED");
        return false;
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvCode, tvCount, tvCoast;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvFbName);
            tvCoast = itemView.findViewById(R.id.tvFbCoast);
            tvCount = itemView.findViewById(R.id.tvFbCount);
        }
    }

}