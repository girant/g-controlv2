package am.artipd.g_controlV2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_LOGIN_WITH_USERNAME;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_LOGOUT;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_RELOGIN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_RETURN_INVALID_LOGIN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_SERVER_ERROR;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.EXTRA_PASSWORD;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.EXTRA_REMEMBER;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.EXTRA_USERNAME;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.LOG_LOG_ACT;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MAIN_PREFS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.PASSWORD;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.PASSWORD_STRING;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.REMEMBER_LOGIN_BOOLEAN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN_PREFS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN_STRING;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.USERNAME;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.USERNAME_STRING;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin, btnRegister;
    EditText etLogin, etPassword;
    SharedPreferences sPrefsToken, sPrefsMain;
    SharedPreferences.Editor spEditToken, spEditMain;
    String INT_ACTIONS = "";
    CheckBox cbRemember;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Check if intent has actions
        if (getIntent().getAction()!=null){
            INT_ACTIONS = getIntent().getAction();
        }

        //attach ids
        btnLogin = findViewById(R.id.btnLoginLogib);
        btnRegister = findViewById(R.id.btnLoginRegister);
        etLogin = findViewById(R.id.etLoginEmail);
        etPassword = findViewById(R.id.etLoginPassword);
        cbRemember = findViewById(R.id.cbRememberme);

        //get preferences
        sPrefsToken = getSharedPreferences(TOKEN_PREFS, MODE_PRIVATE);
        sPrefsMain = getSharedPreferences(MAIN_PREFS, MODE_PRIVATE);
        //USERNAME = sPrefsMain.getString(USERNAME_STRING, "");
        TOKEN = sPrefsToken.getString(TOKEN_STRING, "");
        spEditToken = sPrefsToken.edit();
        spEditMain = sPrefsMain.edit();

        //Check INTENT Actions
        switch (INT_ACTIONS){
            case ACTION_SERVER_ERROR:
                Log.d(LOG_LOG_ACT+"ACTSERVERR", "Action Server error");
                Snackbar.make(findViewById(R.id.loginActCoordLayout), "SERVER ERROR", Snackbar.LENGTH_LONG).show();
                etLogin.setText(USERNAME);
                etPassword.setText(PASSWORD);
                break;
            case ACTION_RETURN_INVALID_LOGIN:
                Log.d(LOG_LOG_ACT+"ACTINVLOGIN", "Action invalid login");
                etLogin.setText(USERNAME);
                etPassword.setText("");
                Snackbar.make(findViewById(R.id.loginActCoordLayout), "Invalid login or password", Snackbar.LENGTH_LONG).show();
                break;
            case ACTION_RELOGIN:
                Log.d(LOG_LOG_ACT+"ACTRELOGIN", "INVAL TOKEN");
                etPassword.setText("");
                spEditMain.remove(PASSWORD_STRING);
                Snackbar.make(findViewById(R.id.loginActCoordLayout), "Invalid token, please try login again", Snackbar.LENGTH_LONG).show();
                break;
            case ACTION_LOGOUT:
                Log.d(LOG_LOG_ACT+"ACTINVLOGOUT", "Action LOGOUT");
                spEditToken.remove(TOKEN_STRING);
                spEditMain.remove(PASSWORD_STRING);
                etLogin.setText(USERNAME);
                spEditToken.apply();
                spEditMain.apply();
            default:break;
        }


        //add listeners
        btnRegister.setOnClickListener(v -> startActivity(new Intent(this, RegisterActivity.class)));
        btnLogin.setOnClickListener(v -> {
            Intent loginSplashIntent = new Intent(this, SplashAndLogin.class);
            //If checked remember checkbox
            if (cbRemember.isChecked()) {
                loginSplashIntent.putExtra(EXTRA_USERNAME, etLogin.getText().toString());
                loginSplashIntent.putExtra(EXTRA_PASSWORD, etPassword.getText().toString());
                loginSplashIntent.putExtra(EXTRA_REMEMBER, true);
            }
            else{
                USERNAME = etLogin.getText().toString();
                PASSWORD = etPassword.getText().toString();
            }
            startActivity(loginSplashIntent.setAction(ACTION_LOGIN_WITH_USERNAME));
            finish();
        });

        if (sPrefsMain.getBoolean(REMEMBER_LOGIN_BOOLEAN, false)) {
            etLogin.setText(sPrefsMain.getString(USERNAME_STRING, ""));
            etPassword.setText(sPrefsMain.getString(PASSWORD_STRING, ""));
            cbRemember.setChecked(true);
        }
    }

}
