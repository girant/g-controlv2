package am.artipd.g_controlV2;

public class UserDataObject {
    String userName, firstName, lastName, email, imageUrl, lanagauage, createdBy, createdDate, authorities;
    boolean isActivated;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLanagauage(String lanagauage) {
        this.lanagauage = lanagauage;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }

    public UserDataObject(String userName, String firstName, String lastName, String email,
                          String imageUrl, String lanagauage, String createdBy, String createdDate,
                          String authorities, boolean isActivated) {

        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.imageUrl = imageUrl;
        this.lanagauage = lanagauage;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.authorities = authorities;
        this.isActivated = isActivated;
    }
    UserDataObject(){

    }
}
