package am.artipd.g_controlV2.psconstants;

public class StaticsAndConstants {
    public static final String MAIN_PREFS = "main_prefs";
    public static final String TOKEN_PREFS = "token_prefs";
    public static final String TOKEN_STRING = "token";
    public static final String REMEMBER_LOGIN_BOOLEAN = "rememberme";
    public static final String USERNAME_STRING = "username";
    public static final String PASSWORD_STRING = "password";
    public static final String EMAIL_STRING = "e-mail";

    //GLOBAL STRINGS
    public static String USERNAME;
    public static String PASSWORD;
    public static String TOKEN;



    //LogTags
    public static final String LOG_LOG_ACT = "GCV2_LogAct";
    public static final String CR_REQ_ACT = "GCV2_CrReq";
    public static final String MA_CONT_ACT = "GCV2_MaContAct";


    //Intent actions
    public static final String ACTION_LOGIN_WITH_USERNAME = "loginWithUsername";
    public static final String ACTION_LOGIN_WITH_TOKEN = "loginWithToken";
    public static final String ACTION_RETURN_INVALID_LOGIN = "invalidLogin";
    public static final String ACTION_SERVER_ERROR = "serverError";
    public static final String ACTION_LOGIN_SUCCESS = "loginSuccess";
    public static final String ACTION_RELOGIN = "reLogin";
    public static final String ACTION_LOGOUT = "logOut";


    //IntentExtras
    public static final String EXTRA_USERNAME = "username";
    public static final String EXTRA_PASSWORD = "password";
    public static final String EXTRA_REMEMBER = "remember_login";

    //Main url
    public static final String MAIN_URL = "http://girant.ddns.net:9000";



}
