package am.artipd.g_controlV2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.CR_REQ_ACT;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MAIN_PREFS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.USERNAME_STRING;

public class CreateRequest {
    static Context cont;
    static String userName;
    public static SharedPreferences spMain;
    public static SharedPreferences.Editor spEditMain;

    public static HttpURLConnection postConnection(String urlString, JSONObject jsonPost, Context context) {
        HttpURLConnection connection = null;


        connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + TOKEN);
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jsonPost.toString());
            Log.d(CR_REQ_ACT+" RESP", jsonPost.toString());
            Log.d(CR_REQ_ACT + " RESP", connection.toString());
            out.close();
        } catch (IOException e) {
            Log.d(CR_REQ_ACT +" ErrPost", e.getMessage());
       //     Toast.makeText(context, "Error connecting to server, \n please try later", Toast.LENGTH_LONG).show();
        }
        return connection;
    }

    public static HttpURLConnection getConnection(String urlString, Context context)  {

        SharedPreferences sPrefsMain = context.getSharedPreferences(MAIN_PREFS, Context.MODE_PRIVATE);
        userName = sPrefsMain.getString(USERNAME_STRING, "");
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
       //     connection.setDoOutput(true);
        //    connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + TOKEN);
        //    connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
       //     OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
      //      out.write(jsonGet.toString());
      //      Log.d("CrResp", jsonGet.toString());
      //      Log.d("CrResp", connection.toString());
      //      out.close();
        } catch (IOException e) {
            Log.d(CR_REQ_ACT + "ERRMETGET", e.getMessage());
        }
        return connection;
    }

    public static HttpURLConnection delConnection(String urlString)  {

        //SharedPreferences sPrefsMain = context.getSharedPreferences(MAIN_PREFS, Context.MODE_PRIVATE);
        //userName = sPrefsMain.getString(USERNAME_STRING, "");
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("DELETE");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + TOKEN);
            connection.setConnectTimeout(15000);
            connection.connect();

        } catch (IOException e) {
            Log.d(CR_REQ_ACT + "ERRMETGET", e.getMessage());
        }
        return connection;
    }

    public static HttpURLConnection updtConnection(String urlString, JSONObject jsonPost, Context context)  {
        HttpURLConnection connection = null;


        //SharedPreferences sPrefsMain = context.getSharedPreferences(MAIN_PREFS, Context.MODE_PRIVATE);
        //userName = sPrefsMain.getString(USERNAME_STRING, "");
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ");
            connection.setRequestProperty("Accept", "*/*");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + TOKEN);
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jsonPost.toString());
            Log.d(CR_REQ_ACT+" RESP", jsonPost.toString());
            Log.d(CR_REQ_ACT + " RESP", connection.toString());
            out.close();
        } catch (IOException e) {
            Log.d(CR_REQ_ACT +" ErrPost", e.getMessage());
            //     Toast.makeText(context, "Error connecting to server, \n please try later", Toast.LENGTH_LONG).show();
        }
        return connection;

    }

    public static boolean checkLogin(HttpURLConnection connection){

        return (getJsonString(connection).equals(userName));
    }

    public static String getJsonString(HttpURLConnection connection){
        InputStream in;
        StringBuilder result = new StringBuilder();
        try {
            in = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            Log.d(CR_REQ_ACT+"METGETRESULT", "result from server: " + result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
