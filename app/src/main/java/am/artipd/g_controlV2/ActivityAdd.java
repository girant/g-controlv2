package am.artipd.g_controlV2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import am.artipd.g_controlV2.DataObjects.ProductDataJson;

import static am.artipd.g_controlV2.CreateRequest.getConnection;
import static am.artipd.g_controlV2.CreateRequest.getJsonString;
import static am.artipd.g_controlV2.CreateRequest.postConnection;
import static am.artipd.g_controlV2.CreateRequest.updtConnection;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MAIN_URL;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MA_CONT_ACT;


public class ActivityAdd extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    Button btnAddItem, btnScanCode;
    EditText etCodeAdd, etNameAdd, etCountAdd, etcoastAdd, etLimit;

    public static final int REQUEST_CODE = 0x0000c0de;
    ProductDataJson pdoJsoonToBeUpdated;
    int responceCode;
    boolean isInBase;
    JSONObject pdoJson = null;

    TextView tvInBase;
    Boolean isFromListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        //Stanum enq fieldneri id
        etCodeAdd = findViewById(R.id.editText_code_number);
        etcoastAdd = findViewById(R.id.editText_coast);
        etCountAdd = findViewById(R.id.editText_count);
        etNameAdd = findViewById(R.id.editText_name);
        btnScanCode = findViewById(R.id.btn_scan_code);
        btnAddItem = findViewById(R.id.buttonAdd);
        tvInBase = findViewById(R.id.tvInBase);
        etLimit = findViewById(R.id.editText_limit);
        if (getIntent().getExtras() == null) {
            isFromListView = false;
            //Ete extra ka stanum enq dranq
        } else {
            isFromListView = getIntent().getExtras().getBoolean("isFromListView");
            String pdoStringToBeUpdated = getIntent().getExtras().getString("product");
            isInBase = true;
            try {
                pdoJsoonToBeUpdated = new ProductDataJson(new JSONObject(pdoStringToBeUpdated));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setFieldsNames(pdoJsoonToBeUpdated);
        }
        Log.d("GCV2_ADDACT_ISFROMLV", isFromListView.toString());

        etNameAdd.setOnFocusChangeListener(this);
        btnAddItem.setOnClickListener(this);
        //avelacnum enq onClickListener barkodi scaneri miacman hamar
        btnScanCode.setOnClickListener(view ->
                new IntentIntegrator(ActivityAdd.this).setOrientationLocked(false).setCaptureActivity(CustomScannerActivity.class).initiateScan());
    }


    //Mshakum enq add kam update button-i onclick@
    @Override
    public void onClick (View view) {

        //Stugum enq, ete bolor fieldner@ lracvac en sharunakum enq kod@
        if (checkIsAllFilled()) {

            if (etcoastAdd.getText().toString().equals("")) {
                etcoastAdd.setText("0");
            }
            if (etLimit.getText().toString().equals("")) {
                etLimit.setText("0");
            }

            try {
                if (!isInBase){
                    pdoJson = new JSONObject();
                    pdoJson.put("name", etNameAdd.getText().toString());
                    pdoJson.put("barcode", etCodeAdd.getText().toString());
                    pdoJson.put("cost", etcoastAdd.getText().toString());
                    pdoJson.put("number", etCountAdd.getText().toString());
                    pdoJson.put("minNumber", etLimit.getText().toString());
                }

                new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] objects) {
                        HttpURLConnection connection = null;
                            if (isInBase){
                                try {
                                    getProductToUpdate();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                // Toast.makeText(getApplicationContext(), "Alredy in base", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                connection = postConnection(MAIN_URL + "/api/products", pdoJson, getApplicationContext());
                                isInBase = false;
                            }


                        try {
                            responceCode = connection.getResponseCode();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        catch (RuntimeException e){
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object o) {
                        super.onPostExecute(o);
                        if (isInBase){
                            clearViews();
                           // setFieldsNames();
                        }
                        switch (responceCode){
                            case 200: clearViews();
                            break;
                            case 201: clearViews();
                                //Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_SHORT).show();
                                break;
                            case 400:
                                //  Toast.makeText(getApplicationContext(), "Internal error please try later", Toast.LENGTH_SHORT).show();
                                break;
                            default:break;

                        }
                    }
                }.execute();


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void getProductToUpdate() throws JSONException {

        pdoJsoonToBeUpdated.setProductName(etNameAdd.getText().toString());
        pdoJsoonToBeUpdated.setProductCoast(Double.valueOf(etcoastAdd.getText().toString()));
        pdoJsoonToBeUpdated.setProductQrCode(etCodeAdd.getText().toString());
        pdoJsoonToBeUpdated.setProductInStockCount(pdoJsoonToBeUpdated.getProductInStockCount() + Double.valueOf(etCountAdd.getText().toString()));
        HttpURLConnection connectToUpdate = updtConnection(MAIN_URL + "/api/products", pdoJsoonToBeUpdated.getJsonObject(), getApplicationContext());
        String info = connectToUpdate.getHeaderField("x-abovyantechapp-alert");
    }


    //Kanchvum e erb scan enq anum shtrixkod@
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult;

        if (requestCode == REQUEST_CODE) {
            scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Scan cancelled", Toast.LENGTH_LONG).show();
            } else {
                //ete scan@ hajox e ancel lracnum enq ardyunq@
                if (scanningResult != null) {
                    etCodeAdd.setText(scanningResult.getContents());
                }
            }
        }


    }



    @Override
    //erb fieldic focus@ poxvum e, setNamesnenq kanchum
    //naxoroq stugelov, vorpeszi codi field@ datark chlini
    public void onFocusChange(View v, boolean hasFocus) {
        String productsName = etNameAdd.getText().toString();
        if(!hasFocus){
            new AsyncTask() {
                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    if (isInBase){
                        setFieldsNames(pdoJsoonToBeUpdated);
                    }
                    else {
                    }
                }
                @Override
                protected Object doInBackground(Object[] objects) {
                    if(checkInBase(productsName)){
                        isInBase = true;
                    }

                    return null;
                }
            }.execute();
        }
        else{
            etNameAdd.setText(etNameAdd.getText().toString());
        }

    }

    //Stugum enq ardyoq bolor fieldern en lracvac te voch
    public boolean checkIsAllFilled() {

        return true;
                //!etNameAdd.getText().toString().equals("") && !etCodeAdd.getText().toString().equals("")
                //&& !etCountAdd.getText().toString().equals("");
    }

    public void clearViews() {
        etCodeAdd.setText("");
        etCountAdd.setText("");
        etNameAdd.setText("");
        etcoastAdd.setText("");
        etLimit.setText("");
        tvInBase.setText("");
    }

    public boolean checkInBase (String productName) {
        HttpURLConnection connectToChack;
        String result = "";
                if (!isFromListView){
                    connectToChack = getConnection(MAIN_URL + "/api/_search/products?query=name=\"" + productName + "\"", getApplicationContext());
                    result = getJsonString(connectToChack);
                    Log.d(MA_CONT_ACT + "Search ", result);

                    if(result.length()>5){
                        isInBase = true;

                        try {
                            JSONArray jsonArray = new JSONArray(result);
                            pdoJsoonToBeUpdated = new ProductDataJson(jsonArray.getJSONObject(0));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(MA_CONT_ACT + "INBASE", "IN BASE");
                        return true;
                    }
                    else {
                        Log.d(MA_CONT_ACT + "INBASE", "IN NOT BASE");
                        return false;
                    }
                }
                else {
                    return true;
                }

    }

    public void setFieldsNames(ProductDataJson pdoJson){
        etCodeAdd.setText(pdoJson.getProductQrCode());
        etNameAdd.setText(pdoJson.getProductName());
        etcoastAdd.setText(String.valueOf(pdoJson.getProductCoast()));
        btnAddItem.setText("Update item");
        tvInBase.setText(String.valueOf(pdoJson.getProductInStockCount()));
        etLimit.setText(String.valueOf(pdoJson.getProductCountLimitation()));
    }
}
