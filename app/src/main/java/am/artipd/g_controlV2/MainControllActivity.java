package am.artipd.g_controlV2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.os.Parcelable;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import am.artipd.g_controlV2.DataObjects.ProductDataJson;
import am.artipd.g_controlV2.DataObjects.ProductDataObject;

import static am.artipd.g_controlV2.CreateRequest.delConnection;
import static am.artipd.g_controlV2.CreateRequest.getConnection;
import static am.artipd.g_controlV2.CreateRequest.getJsonString;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_LOGOUT;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.EMAIL_STRING;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MAIN_PREFS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MAIN_URL;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MA_CONT_ACT;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.REMEMBER_LOGIN_BOOLEAN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN_PREFS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN_STRING;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.USERNAME_STRING;

public class MainControllActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    SharedPreferences sPrefsToken, sPrefsMain;
    SharedPreferences.Editor spEditToken, spEditMain;
    //ProductListRecyclerViewAdapter adapter;
    ProductListRecyclerViewJsonAdapter adapter;
    String userName, email;
    TextView tvUserName, tvEmail;
    UserDataObject uda;
    //ProductDataObject pdo;
    ProductDataJson pdoJson;
    ArrayList<ProductDataJson> pdJsonList;
    //ArrayList<ProductDataObject> productDataObjectArrayList;
    ProgressBar pbGetUserInfo;
    RecyclerView recProductsView;
    FloatingActionsMenu floatMenu;
    public static final int REQUEST_EDIT = 2;
    HttpURLConnection deleteItemConnection;



    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 0:
                //getProducts();
                Toast.makeText(getApplicationContext(), String.valueOf(item.getGroupId()), Toast.LENGTH_SHORT).show();
                deleteItem(item.getGroupId());
                break;
            case 1:
                updateItem(item.getGroupId());
             //   EditEntry(item.getGroupId());
                break;
            case 2:
             //   takeFromBase(item.getGroupId());
            default: break;
        }
        return super.onContextItemSelected(item);

    }

    private void deleteItem(int groupId) {
       // int idToBeDeleted = productDataObjectArrayList.get(groupId).getID();
        int idToBeDeleted = pdJsonList.get(groupId).getID();

        //Toast.makeText(this, String.valueOf(idToBeDeleted), Toast.LENGTH_SHORT);
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                deleteItemConnection = delConnection(MAIN_URL + "/api/products/" + idToBeDeleted);
                try {
                    Log.d(MA_CONT_ACT + "DEL", String.valueOf(deleteItemConnection.getResponseCode()));
                    Log.d(MA_CONT_ACT + "DEL", deleteItemConnection.getResponseMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                try{
                    switch (deleteItemConnection.getResponseCode()){
                        //case 200: productDataObjectArrayList.remove(groupId);
                        case 200: pdJsonList.remove(groupId);
                            adapter.notifyItemRemoved(groupId);
                            break;
                        case 204: Toast.makeText(getApplicationContext(), "Nothing found", Toast.LENGTH_SHORT).show();
                        adapter.notifyDataSetChanged();
                        break;
                        default:Toast.makeText(getApplicationContext(), "Server error", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    Log.d(MA_CONT_ACT + "delerr", "ERROR CODE" + deleteItemConnection.getResponseCode());
                    Log.d(MA_CONT_ACT + "delerr", "ERROR CODE" + deleteItemConnection.getResponseMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }.execute();
    }

    private void updateItem(int groupId) {
        ProductDataJson pdoUpdate = pdJsonList.get(groupId);
        String itemName = pdoUpdate.getProductName();
        Toast.makeText(getApplicationContext(), itemName, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ActivityAdd.class);
        intent.putExtra("product", pdoUpdate.getJsonToSttring());
        intent.putExtra("isFromListView", true);
        startActivity(intent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_controll);


        //attach ids
        pbGetUserInfo = findViewById(R.id.pbGetUserInfo);
        pbGetUserInfo.setVisibility(View.INVISIBLE);

        recProductsView = findViewById(R.id.recyclerWithHolder);
        recProductsView.setHasFixedSize(true);
        LinearLayoutManager linLayManager = new LinearLayoutManager(this);
        linLayManager.setOrientation(LinearLayoutManager.VERTICAL);
        recProductsView.setLayoutManager(linLayManager);

        sPrefsToken = getSharedPreferences(TOKEN_PREFS, MODE_PRIVATE);
        sPrefsMain = getSharedPreferences(MAIN_PREFS, MODE_PRIVATE);
        spEditToken = sPrefsToken.edit();
        spEditMain = sPrefsMain.edit();
        TOKEN = sPrefsToken.getString(TOKEN_STRING, "");
        uda = new UserDataObject("", "", "", "", "", "", "", "", "", false);
        userName = sPrefsMain.getString(USERNAME_STRING, "");
        email = sPrefsMain.getString(EMAIL_STRING, "");

        //productDataObjectArrayList = new ArrayList<>();
        pdJsonList = new ArrayList<>();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        floatMenu = findViewById(R.id.fabMenu);
        final com.getbase.floatingactionbutton.FloatingActionButton fabAdd = findViewById(R.id.fabAdd);
        final com.getbase.floatingactionbutton.FloatingActionButton fabTake = findViewById(R.id.fabTake);
        fabTake.setOnClickListener(this);
        fabAdd.setOnClickListener(this);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        tvUserName = headerView.findViewById(R.id.navbarUsername);
        tvEmail = headerView.findViewById(R.id.nnavbarEmail);
        getUser();
        getProducts();
        adapter = new ProductListRecyclerViewJsonAdapter(pdJsonList);
        recProductsView.setAdapter(adapter);
        recProductsView.setOnTouchListener((v, event) -> {
            if(floatMenu.isExpanded()){
                floatMenu.collapse();
            }
            return false;
        });

        recProductsView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(floatMenu.isExpanded()){
                    floatMenu.collapse();
                }
                super.onScrolled(recyclerView, dx, dy);
                if(dy>0){
                    if (floatMenu.isShown()){

                        floatMenu.setVisibility(View.INVISIBLE);
                    }
                }
                else floatMenu.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProducts();
        tvEmail.setText(uda.email);
        tvUserName.setText("HI " + uda.userName);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_controll, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            pbGetUserInfo.setVisibility(View.VISIBLE);

            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            TOKEN = "";
            spEditMain.putBoolean(REMEMBER_LOGIN_BOOLEAN, false);


            //spEditMain.remove(USERNAME_STRING);
            spEditMain.apply();
            startActivity(new Intent(this, LoginActivity.class).setAction(ACTION_LOGOUT));
            finish();

        } else if (id == R.id.nav_slideshow) {
            pbGetUserInfo.setVisibility(View.GONE);



        } else if (id == R.id.nav_tools) {
            pbGetUserInfo.setVisibility(View.INVISIBLE);


        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getProducts(){
        AsyncTask getProductsListTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                parseProductsList(getJsonString(getConnection(MAIN_URL + "/api/products", getApplicationContext())));
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                adapter.notifyDataSetChanged();
            }
        };
        getProductsListTask.execute();
    }

    public void getUser(){
        pbGetUserInfo.setVisibility(View.VISIBLE);
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                parseJson(getJsonString(getConnection(MAIN_URL + "/api/account", getApplicationContext())));
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                pbGetUserInfo.setVisibility(View.INVISIBLE);
                adapter.notifyDataSetChanged();
            }

            @Override
            protected void onProgressUpdate(Object[] values) {
                super.onProgressUpdate(values);
            }
        };
        asyncTask.execute();
       // new Thread(()->{
    //        parseJson(getJsonString(getConnection("http://girant.ddns.net:9000/api/account", getApplicationContext())));
    //    }).start();
        //
    }

    public void parseJson(String jsonUserObject){
        try {
            JSONObject jsonObject = new JSONObject(jsonUserObject);
            uda.setEmail(jsonObject.getString("email"));
            uda.setFirstName(jsonObject.getString("firstName"));
            uda.setEmail(jsonObject.getString("email"));
            uda.setUserName(jsonObject.getString("login"));


            Log.d(MA_CONT_ACT, "parseJson: " + jsonObject.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void parseProductsList(String jsonProductsList){
        if (pdJsonList.size()>0){
            pdJsonList.clear();
        }
        try {
            JSONObject jsonProduct;
            JSONArray jsonArray = new JSONArray(jsonProductsList);
            for(int i=0; i<jsonArray.length(); i++){
                jsonProduct = jsonArray.getJSONObject(i);
                pdoJson = new ProductDataJson(jsonProduct);
                pdJsonList.add(pdoJson);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fabAdd:
                Intent intAddActivity = new Intent(this, ActivityAdd.class);
                startActivityForResult(intAddActivity, REQUEST_EDIT);
                break;
            case R.id.fabTake:
               // intAddActivity = new Intent(this, TakeActivity.class);
               // startActivityForResult(intAddActivity, REQUEST_EDIT);
                break;
            default:break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onResume();
    }
}
