package am.artipd.g_controlV2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import static am.artipd.g_controlV2.CreateRequest.checkLogin;
import static am.artipd.g_controlV2.CreateRequest.getConnection;
import static am.artipd.g_controlV2.CreateRequest.postConnection;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_LOGIN_SUCCESS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_LOGIN_WITH_USERNAME;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_RELOGIN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_RETURN_INVALID_LOGIN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.ACTION_SERVER_ERROR;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.EXTRA_PASSWORD;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.EXTRA_REMEMBER;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.EXTRA_USERNAME;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.LOG_LOG_ACT;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MAIN_PREFS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.MAIN_URL;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.PASSWORD;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.PASSWORD_STRING;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.REMEMBER_LOGIN_BOOLEAN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN_PREFS;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.TOKEN_STRING;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.USERNAME;
import static am.artipd.g_controlV2.psconstants.StaticsAndConstants.USERNAME_STRING;

public class SplashAndLogin extends AppCompatActivity {
    SharedPreferences spMain, spToken;
    SharedPreferences.Editor spEditToken, spEditMain;
    String token, userName, password;
    boolean ifRemember;
    boolean remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_and_login);

        Intent intent = getIntent();

        spToken = getSharedPreferences(TOKEN_PREFS, MODE_PRIVATE);
        spMain = getSharedPreferences(MAIN_PREFS, MODE_PRIVATE);
        spEditMain = spMain.edit();
        spEditToken = spToken.edit();
        TOKEN = spToken.getString(TOKEN_STRING, "");
        if (intent.hasExtra(EXTRA_USERNAME)){
            userName = getIntent().getStringExtra(EXTRA_USERNAME);
        }
        else userName = USERNAME;
        if (intent.hasExtra(EXTRA_PASSWORD)){
            password = getIntent().getStringExtra(EXTRA_PASSWORD);
        }
        else password = PASSWORD;
        if (intent.hasExtra(EXTRA_REMEMBER)){
            ifRemember = getIntent().getBooleanExtra(EXTRA_REMEMBER, false);
        }
        remember = spMain.getBoolean(REMEMBER_LOGIN_BOOLEAN, false);


        String activityAction = getIntent().getAction();

        switch (activityAction){
            case ACTION_LOGIN_WITH_USERNAME:
                new Thread(() -> {
                    HttpURLConnection connection;
                    JSONObject jsonPost = new JSONObject();
                    try {
                        jsonPost.put("password", password);
                        jsonPost.put("rememberMe", ifRemember);
                        jsonPost.put("username", userName);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    connection = postConnection(MAIN_URL+"/api/authenticate", jsonPost, getApplicationContext());

                    try {
                        Log.d(LOG_LOG_ACT+"Responce", connection.getResponseMessage());
                        Log.d(LOG_LOG_ACT+"Responce", String.valueOf(connection.getResponseCode()));

                        switch (connection.getResponseCode()) {
                            case 401:
                               // Snackbar.make(findViewById(R.id.loginPageLinearLayout), "Invalid user or password", Snackbar.LENGTH_LONG).show();
                                startActivity(new Intent(this, LoginActivity.class).setAction(ACTION_RETURN_INVALID_LOGIN));
                                finish();
                                break;
                            case 200:
                              //  Snackbar.make(findViewById(R.id.loginPageLinearLayout), "Login success", Snackbar.LENGTH_LONG).show();
                                InputStream in = new BufferedInputStream(connection.getInputStream());
                                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                                StringBuilder result = new StringBuilder();
                                String line;
                                while ((line = reader.readLine()) != null) {
                                    result.append(line);
                                }
                                Log.d(LOG_LOG_ACT+"ResultServer", "result from server: " + result.toString());
                                token = result.substring(result.indexOf(":") + 2, result.lastIndexOf("}") - 1);
                                Log.d(LOG_LOG_ACT+" Token", token);
                                spEditToken.putString(TOKEN_STRING, token);
                                if(ifRemember){
                                    spEditMain.putString(PASSWORD_STRING, password);
                                    spEditMain.putString(USERNAME_STRING, userName);
                                    spEditMain.putBoolean(REMEMBER_LOGIN_BOOLEAN, true);
                                    spEditMain.apply();
                                }
                                spEditToken.apply();
                                startActivity(new Intent(this, MainControllActivity.class).setAction(ACTION_LOGIN_SUCCESS));
                                finish();
                                break;
                            default:
                                startActivity(new Intent(this, LoginActivity.class).setAction(ACTION_SERVER_ERROR));
                                finish();
                                break;
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.d(LOG_LOG_ACT+"EXCEPTION", e.getMessage());
                        startActivity(new Intent(this, LoginActivity.class).setAction(ACTION_SERVER_ERROR));
                        finish();
                    } finally {
                        if (connection != null) {
                            connection.disconnect();
                        }
                    }
                }).start();
                break;
                default:
                    if(remember){
                        new Thread(()->{
                            if(checkLogin(getConnection(MAIN_URL+"/api/authenticate", getApplicationContext()))) {
                                startActivity(new Intent(this, MainControllActivity.class).setAction(ACTION_LOGIN_SUCCESS));
                                finish();
                            }
                            else{
                                startActivity(new Intent(this, LoginActivity.class).setAction(ACTION_RELOGIN));
                                finish();
                            }
                        }).start();
                    }
                    else {
                        startActivity(new Intent(this, LoginActivity.class));
                        finish();
                    }
                    break;
        }

        }
}
